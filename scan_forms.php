<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/lib/formslib.php');

class archive_scan_form extends moodleform {

    function definition() {
        global $CFG;

        $mform    = $this->_form;

        $archives = $this->_customdata['archives'];

        /// form definition
        //--------------------------------------------------------------------------------

        $mform->addElement('header','selectarchives', get_string('selectarchives', 'local_archive'));

        sort($archives);
        foreach ($archives as $archiveid=>$archive) {
            $elementName = 'archive_'.$archive->get_id();
            $mform->addElement('checkbox', $elementName, $archive->get_name());
            $mform->setDefault($elementName, true);
        }
        $this->add_action_buttons(true, get_string('scanbutton', 'local_archive'));
    }
}
