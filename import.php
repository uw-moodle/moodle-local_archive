<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/local/archive/locallib.php');
require_once($CFG->dirroot . '/local/archive/ui_component.php');
require_once($CFG->dirroot . '/local/archive/restore_extensions.php');
require_once($CFG->dirroot . '/local/archive/renderer.php');

// The courseid we are importing to
$courseid = required_param('id', PARAM_INT);
// The id of the archive we are importing FROM
$importid = optional_param('importid', false, PARAM_INT);
// The target method for the restore (adding or deleting)
//$restoretarget = optional_param('target', backup::TARGET_CURRENT_ADDING, PARAM_INT);
$restoretarget = optional_param('target', backup::TARGET_NEW_COURSE, PARAM_INT);

// Load the course and context
$course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
$context = context_course::instance($courseid);

// Must pass login
require_login($course);
// Must hold restoretargetimport in the current course
require_capability('moodle/restore:restoretargetimport', $context);

$heading = get_string('import');

$thisurl = new moodle_url('/local/archive/import.php', array('id'=>$courseid));

// Set up the page
$PAGE->set_title($heading);
$PAGE->set_heading($heading);
$PAGE->set_url($thisurl);
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');

// Prepare the backup renderer
$renderer = $PAGE->get_renderer('local_archive');

// Check for cancel
if (optional_param('cancel', false, PARAM_ALPHA)) {
    $url = new moodle_url('/course/view.php', array('id'=>$courseid));
    redirect($url);
}

echo $OUTPUT->header();

// Various form elements.  Only some will be defined for each stage.
$customize = optional_param('importcustom', false, PARAM_ALPHA);
$previous = optional_param('previous', false, PARAM_ALPHA);
$search = optional_param('searchcourses', false, PARAM_BOOL);
$importfull = optional_param('importfull', false, PARAM_BOOL);
$importcustom = optional_param('importcustom', false, PARAM_BOOL);

$doimport = $importfull || $importcustom;

if ($previous && !$doimport) {
    $importid = false;
}

// Check if we already have a import course id and submit was pressed
if ($importid === false || $search) {
    // Obviously not... show the selector so one can be chosen
    $search = new archive_course_search(array('pageparams'=>array('id'=>$courseid)));

    // show the course selector
    echo $renderer->archive_import_course_selector($search, $PAGE->url);
    echo $OUTPUT->footer();
    die();
}

// Load the archive course to import from
$importcourse = $DB->get_record('archive_course', array('id'=>$importid), '*', MUST_EXIST);

// Confirm import
if (!$doimport) {

    $details = array('coursename' => $importcourse->coursename,
                     'archivedate' => $importcourse->archivedate,
                     'archivesize' => $importcourse->archivesize,
                     'canrestoreusers' => has_capability('moodle/restore:userinfo', $context));
    $nextparams = array('sesskey'=>sesskey(), 'importid'=>$importid, 'targetid'=>$courseid);
    echo $renderer->backup_details_confirm($thisurl, $nextparams, $details);
    echo $OUTPUT->footer();
    die;
}

confirm_sesskey();

// Make sure the user can backup from that archive
if (!local_archive_user_can_import($USER, $importid)) {
    print_error("No permission to restore archive.");
    die();
}

if (!$importcustom) {
    // Do a non-interactive import.

    echo html_writer::start_div('', array('id' => 'executionprogress'));
    // Start the progress display.
    $progress = new \core\progress\display();

    // Prepare logger for import.
    $logger = new core_backup_html_logger($CFG->debugdeveloper ? backup::LOG_DEBUG : backup::LOG_INFO);

    $enableuserinfo = optional_param('enableuserinfo', false, PARAM_BOOL);
    if ($enableuserinfo) {
        require_capability('moodle/restore:userinfo', $context);
    }
    $warning = local_archive_execute_restore($courseid, $importid, $restoretarget, null, $enableuserinfo, $progress, $logger);

    // All progress complete. Hide progress area.
    echo html_writer::end_div();
    echo html_writer::script('document.getElementById("executionprogress").style.display = "none";');

    echo $OUTPUT->notification(get_string('importsuccess', 'backup'), 'notifysuccess');
    echo $OUTPUT->continue_button(new moodle_url('/course/view.php', array('id'=>$course->id)));

    // Get and display log data if there was any.
    $loghtml = $logger->get_html();
    if ($loghtml != '') {
        echo $renderer->log_display($loghtml);
    }

    echo $OUTPUT->footer();

    die();

}

// Interactive import code starts here.
// This is based on backup/restore.php

$stage       = optional_param('stage', restore_ui::STAGE_CONFIRM, PARAM_INT);
$context     = context_course::instance($course->id);

// Prepare a progress bar which can display optionally during long-running
// operations while setting up the UI.
$slowprogress = new \core\progress\display_if_slow(get_string('preparingui', 'backup'));

// Overall, allow 10 units of progress.
$slowprogress->start_progress('', 10);

// This progress section counts for loading the restore controller.
$slowprogress->start_progress('', 1, 1);

// Restore of large courses requires extra memory. Use the amount configured
// in admin settings.
raise_memory_limit(MEMORY_EXTRA);

if ($stage == restore_ui::STAGE_CONFIRM) {
    // Engage our custom confirm stage.
    $restore = new local_archive_ui_stage_confirm($context->id);
} else {
    // Confirm stage passes control to the standard restore UI, so we shouldn't get here.
    throw new coding_exception('Unknown stage');
}

// End progress section for loading restore controller.
$slowprogress->end_progress();

// This progress section is for the 'process' function below.
$slowprogress->start_progress('', 1, 9);

// Depending on the code branch above, $restore may be a restore_ui or it may
// be a restore_ui_independent_stage. Either way, this function exists.
$restore->set_progress_reporter($slowprogress);
$outcome = $restore->process();

$loghtml = '';
// Finish the 'process' progress reporting section, and the overall count.
$slowprogress->end_progress();
$slowprogress->end_progress();

echo $renderer->progress_bar($restore->get_progress_bar());
echo $restore->display($renderer);
$restore->destroy();
unset($restore);

// Display log data if there was any.
if ($loghtml != '') {
    echo $renderer->log_display($loghtml);
}

echo $OUTPUT->footer();
