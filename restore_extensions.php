<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains extension of the backup classes that override some methods
 * and functionality in order to customise the backup UI for the purposes of
 * import.
 *
 * @package   local_archive
 * @copyright 2015 University of Wisconsin - Madison
 * @author    Matt Petro
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


class local_archive_ui_stage_confirm extends restore_ui_independent_stage implements file_progress {
    /**
     * The context ID.
     * @var int
     */
    protected $contextid;

    /**
     * The course ID.
     * @var int
     */
    protected $courseid;


    /**
     * The archive ID.
     * @var int
     */
    protected $archiveid;

    /**
     * The target type.
     * @var int
     */
    protected $target;

    /**
     * Allow user info to be restored.
     * @var int
     */
    protected $enableuserinfo;

    /**
     * @var array
     */
    protected $details;

    /**
     * @var bool True if we have started reporting progress
     */
    protected $startedprogress = false;

    /**
     * Constructor
     * @param int $contextid
     * @throws coding_exception
     */
    public function __construct($contextid) {
        $this->contextid = $contextid;
        // The following should be passed as params, but the base class constructor doesn't provide for that.
        $this->courseid = required_param('id', PARAM_INT);
        $this->archiveid = required_param('importid', PARAM_INT);
        $this->target = required_param('target', PARAM_INT);

        // The built-in restore system is checking userinfo restore caps, so no need to validate user really
        // has this access.
        $this->enableuserinfo = optional_param('enableuserinfo', false, PARAM_BOOL);
    }

    /**
     * Processes this restore stage
     * @return bool
     * @throws restore_ui_exception
     */
    public function process() {
        global $CFG, $USER;

        // Extract or copy the archive

        $this->filepath = restore_controller::get_tempdir_name($this->contextid, $USER->id);
        if (!is_dir("$CFG->tempdir/backup")) {
            mkdir("$CFG->tempdir/backup",$CFG->directorypermissions);
        }
        $tempdestination = "$CFG->tempdir/backup/$this->filepath";
        list($res,$ft) = local_archive_extract_archive_to_dir($this->archiveid, $tempdestination, $this);

        // If userinforestore is not enabled, then edit backup to remove userinfo flag.
        // We do this so that our default is to restore without userinfo, which is what people will want.
        // Restore doesn't give us the option to set the default in the UI.
        if (!$this->enableuserinfo) {
            $format = backup_general_helper::detect_backup_format($this->filepath);

            // We only have to handle the moodle2 case, since other formats probably won't restore user data.
            if ($format == backup::FORMAT_MOODLE) {
                $moodlexml   = $tempdestination . '/moodle_backup.xml';

                if (!file_exists($moodlexml)) {
                    throw new moodle_exception('Unable to load moodle_backup.xml');
                }
                rename($moodlexml, "$moodlexml.sav");
                // It would be handy to use XSL here, but that's adding a new dependency to moodle.
                $xml = simplexml_load_file("$moodlexml.sav");
                if (!$xml) {
                    throw new moodle_exception('Unable to load moodle_backup.xml');
                }
                // Set all userdata settings to 0.
                foreach ($xml->information->settings->setting as $setting) {
                    $name = $setting->name;
                    if ($name == 'users' || $name == 'role_assignments' || $name == 'userscompletion' || $name == 'grade_histories' || preg_match('/.*_userinfo$/', $name)) {
                        $setting->value = 0;
                    }
                }
                $xml->asXml($moodlexml);

            }
        }

        // If any progress happened, end it.
        if ($this->startedprogress) {
            $this->get_progress_reporter()->end_progress();
        }
        return $res;
    }

    /**
     * Implementation for file_progress interface to display unzip progress.
     *
     * @param int $progress Current progress
     * @param int $max Max value
     */
    public function progress($progress = file_progress::INDETERMINATE, $max = file_progress::INDETERMINATE) {
        $reporter = $this->get_progress_reporter();

        // Start tracking progress if necessary.
        if (!$this->startedprogress) {
            $reporter->start_progress('extract_file_to_dir',
                    ($max == file_progress::INDETERMINATE) ? \core\progress\base::INDETERMINATE : $max);
            $this->startedprogress = true;
        }

        // Pass progress through to whatever handles it.
        $reporter->progress(
                ($progress == file_progress::INDETERMINATE) ? \core\progress\base::INDETERMINATE : $progress);
    }

    /**
     * Renders the confirmation stage screen
     *
     * @param core_backup_renderer $renderer renderer instance to use
     * @return string HTML code
     */
    public function display(core_backup_renderer $renderer) {

        $prevstageurl = new moodle_url('/local/archive/import.php', array('id' => $this->courseid));
        $nextstageurl = new moodle_url('/backup/restore.php', array(
                'contextid'     => $this->contextid,
                'filepath'      => $this->filepath,
                'target'        => $this->target,
                'targetid'      => $this->courseid,
                'importcustom'  => 1,
                'stage'         => restore_ui::STAGE_SETTINGS));

        $format = backup_general_helper::detect_backup_format($this->filepath);

        if ($format === backup::FORMAT_UNKNOWN) {
            // Unknown format - we can't do anything here.
            return $renderer->backup_details_unknown($prevstageurl);

        } else if ($format !== backup::FORMAT_MOODLE) {
            // Non-standard format to be converted.
            $details = array('format' => $format, 'type' => backup::TYPE_1COURSE); // todo type to be returned by a converter
            return $renderer->backup_details_nonstandard($nextstageurl, $details);

        } else {
            // Standard MBZ backup, let us get information from it and display.
            $this->details = backup_general_helper::get_backup_information($this->filepath);
            return $renderer->backup_details($this->details, $nextstageurl);
        }
    }

    /**
     * The restore stage name.
     * @return string
     * @throws coding_exception
     */
    public function get_stage_name() {
        return get_string('restorestage'.restore_ui::STAGE_CONFIRM, 'backup');
    }

    /**
     * The restore stage this class is for.
     * @return int
     */
    public function get_stage() {
        return restore_ui::STAGE_CONFIRM;
    }
}
