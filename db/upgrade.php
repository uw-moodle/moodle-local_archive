<?php
/**
 * Keeps track of upgrades to the local_archive plugin
 */

/**
 * @param int $oldversion the version we are upgrading from
 * @return bool result
 */
function xmldb_local_archive_upgrade($oldversion) {
    global $DB, $CFG, $OUTPUT;

    $dbman = $DB->get_manager();

    $result = true;

    if ($oldversion < 2012042700) {

        // Define field archivesize to be added to archive_course
        $table = new xmldb_table('archive_course');
        $field = new xmldb_field('archivesize', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null, 'data');

        // Conditionally launch add field archivesize
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // archive savepoint reached
        upgrade_plugin_savepoint(true, 2012042700, 'local', 'archive');
    }

    if ($oldversion < 2014040900) {

        // Define field lastscan to be added to archive_course
        $table = new xmldb_table('archive_course');
        $field = new xmldb_field('lastscan', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0', 'archivesize');

        // Conditionally launch add field lastscan
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // archive savepoint reached
        upgrade_plugin_savepoint(true, 2014040900, 'local', 'archive');
    }


    return $result;
}
