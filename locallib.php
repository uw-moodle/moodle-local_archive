<?php

define('LOCAL_ARCHIVE_TEACHER_ROLE', 1);

require_once($CFG->dirroot.'/local/archive/lib.php');
require_once($CFG->dirroot.'/local/archive/storagetype/base.php');
require_once($CFG->dirroot.'/local/archive/storagetype/moodle.php');
require_once($CFG->dirroot.'/local/archive/storagetype/moodle1.php');
require_once($CFG->dirroot.'/local/archive/storagetype/moodle2.php');
require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');
require_once($CFG->dirroot.'/enrol/wisc/lib.php');

/**
 * Contains a list of all active archive instances.
 */
$LOCAL_ARCHIVE_INSTANCES = false;

function local_archive_init_archives() {
    global $CFG, $LOCAL_ARCHIVE_INSTANCES;
    if ($LOCAL_ARCHIVE_INSTANCES === false) {
        $config = local_archive_read_config();
        if (empty($config)) {
            $LOCAL_ARCHIVE_INSTANCES = array();
        } else {
            foreach ($config as $archivedef) {
                $archive = local_archive_get_storageclass($archivedef);
                $LOCAL_ARCHIVE_INSTANCES[$archive->get_id()] = $archive;
            }
        }
    }
}

function local_archive_read_config() {
    global $CFG, $LOCAL_ARCHIVE_INSTANCES;
    if (!isset($CFG->local_archive_config)) {
        $CFG->local_archive_config = array();
    }
    if (!empty($CFG->local_archive_includes)) {
        foreach ($CFG->local_archive_includes as $filename) {
            include_once($filename);
        }
    }
    return $CFG->local_archive_config;
}

function local_archive_get_storageclass($config) {
    if (is_object($config)) {
        $config = (array)$config;
    }
    $classname = 'archive_storagetype_'.$config['type'];
    if (!class_exists($classname)) {
        throw new moodle_exception("local_archive: Bad type: {$config['type']}");
    }
    return new $classname($config);
}

function local_archive_update_index($instancenames = array()) {
    global $CFG, $LOCAL_ARCHIVE_INSTANCES;
    global $DB;
    local_archive_init_archives();
    foreach ($LOCAL_ARCHIVE_INSTANCES as $archive) {
        if (!empty($instancenames)) {
            if (false === array_search($archive->get_name(), $instancenames)) {
                continue;  // skip this archive
            }
        }
        mtrace("Updating ".$archive->get_name()." (".$archive->get_storagetype().")... ");
        try {
            $archive->update_index();
        } catch (Exception $e) {
            echo "Exception: ".$e->getMessage();
            error_log('local/archive/'.$archive->get_storagetype().':'.$e->getMessage());
        }
        $count = $DB->count_records('archive_course', array('archiveid'=>$archive->get_id()));
        mtrace("$count courses<br/>");
    }
}

function local_archive_user_can_import($user, $acourseid) {
    global $DB;
    $systemcontext = context_system::instance();
    if (has_capability('local/archive:anyarchive', $systemcontext, $user)) {
        return true;
    }
    $ausers = array_keys(local_archive_find_matching_users($user));
    if (!empty($ausers)) {
        list($usql, $params) = $DB->get_in_or_equal($ausers, SQL_PARAMS_NAMED);
        $select = "acourseid = :acourseid AND auserid $usql";
        $params['acourseid'] = $acourseid;
        if ($DB->count_records_select('archive_user_assign', $select, $params)) {
            return true;
        }
    }
    return false;
}

function local_archive_find_matching_users($user) {
    $data = new stdClass();
    $netid = false;
    $pvi = false;
    $authtype = get_config('local_wiscservices', 'authtype');
    if ($user->auth === $authtype) {
        $netid = null;
        $pvi = null;
        list($username, $domain) = explode('@',$user->username, 2);
        if ($domain === 'wisc.edu') {
            $netid = $username;
            $pvi = $user->idnumber;
        }
    }
    if ($netid) {
        $data->netid = $netid;
    }
    if ($pvi) {
        $data->pvi = $pvi;
    }
    $ausers = local_archive_find_users($data);
    return $ausers;
}

function local_archive_find_users($data) {
    global $DB;
    $selects = array();
    $params = array();
    if (!empty($data->pvi)) {
        $users = $DB->get_records('archive_user', array('pvi'=>$data->pvi));
    } else if (!empty($data->netid)) {
        $users = $DB->get_records('archive_user', array('netid'=>$data->netid));
    } else if (!empty($data->externalidinfo) && !empty($data->externalid)) {
        $users = $DB->get_records('archive_user', array('externalidinfo'=>$data->externalidinfo, 'externalid'=>$data->externalid));
    } else {
        $users = array();
    }
    // make sure all non-empty identifiers agree
    $allmatches = array();
    foreach ($users as $user) {
        $match = true;
        if (!empty($user->pvi)) {
            $match &= $user->pvi === $data->pvi;
        }
        if (!empty($user->netid)) {
            $match &= $user->netid === $data->netid;
        }
        if (!empty($user->externalidinfo) && !empty($user->externalid)) {
            $match &= $user->externalidinfo === $data->externalidinfo;
            $match &= $user->externalid === $data->externalid;
        }
        if ($match) {
            $allmatches[$user->id] = $user;
        }
    }
    return $allmatches;
}

function local_archive_save_course($acourse, $coursemaps, $assignments) {
    global $DB;
    $rec = $DB->get_record('archive_course', array('archiveid'=>$acourse->archiveid, 'data'=>$acourse->data), 'id');
    if (!$rec) {
        $acourse->id = $DB->insert_record('archive_course', $acourse);
    } else {
        $acourse->id = $rec->id;
        $DB->update_record('archive_course', $acourse);
    }
    local_archive_save_coursemaps($acourse->id, $coursemaps);
    local_archive_save_users($acourse->id, $assignments);
    return $acourse->id;
}

function local_archive_save_coursemaps($acourseid, $maps) {
    global $DB;
    $cmp = 'local_archive_coursemap_cmp';
    $dbmaps = $DB->get_records('archive_coursemap', array('acourseid'=>$acourseid));
    // delete obsolete maps
    $obsoleteids = array_keys(array_udiff($dbmaps, $maps, $cmp));
    if ($obsoleteids) {
        $DB->delete_records_list('archive_coursemap', 'id', $obsoleteids);
    }
    // insert new maps
    $todo = array_udiff($maps, $dbmaps, $cmp);
    foreach ($todo as $map) {
        $map->acourseid = $acourseid;
        $map->id = $DB->insert_record('archive_coursemap', $map);
    }
}

function local_archive_save_users($acourseid, $assignments) {
    global $DB;
    $cmp = 'local_archive_user_assign_cmp';
    $recs = array();
    foreach ($assignments as $assign) {
        $role = $assign['role'];
        $userdata = $assign['user'];
        $userid = local_archive_update_user($userdata);
        if ($userid === false) {
            continue;
        }
        $recs[] = (object) array('acourseid'=>$acourseid, 'auserid'=>$userid, 'archiverole'=>$role);
    }
    $dbrecs = $DB->get_records('archive_user_assign', array('acourseid'=>$acourseid));
    // delete obsolete records
    $obsoleteids = array_keys(array_udiff($dbrecs, $recs, $cmp));
    if ($obsoleteids) {
        $DB->delete_records_list('archive_user_assign', 'id', $obsoleteids);
    }
    // insert new records
    $todo = array_udiff($recs, $dbrecs, $cmp);
    foreach ($todo as $rec) {
        $rec->id = $DB->insert_record('archive_user_assign', $rec);
    }
}

function local_archive_update_user($data) {
    global $DB;
    if (empty($data->netid) && empty($data->pvi) && empty($data->externalid)) {
        return false;  // we require at least one identifier
    }
    $users = local_archive_find_users($data);
    $user = reset($users);  // use the first match, I guess
    if (!$user) {
        // insert new user
        $id = $DB->insert_record('archive_user', $data);
    } else {
        // update existing user, being careful not to overwrite existing data with null
        $fields = array('pvi', 'netid', 'externalidinfo', 'externalid', 'firstname', 'lastname', 'email');
        foreach ($fields as $field) {
            if (!empty($data->{$field}) && $data->{$field} !== $user->{$field}) {
                $DB->set_field('archive_user', $field, $data->{$field}, array('id'=>$user->id));
            }
        }
        $id = $user->id;
    }
    return $id;
}

function local_archive_delete_course($acourseid) {
    global $DB;
    $DB->delete_records('archive_coursemap', array('acourseid'=>$acourseid));
    $DB->delete_records('archive_user_assign', array('acourseid'=>$acourseid));
    $DB->delete_records('archive_course', array('id'=>$acourseid));
}

function local_archive_extract_archive_to_dir($acourseid, $filepath, file_progress $fileprogress = null) {
    global $DB, $LOCAL_ARCHIVE_INSTANCES;
    local_archive_init_archives();
    $archiveid = $DB->get_field('archive_course', 'archiveid', array('id' => $acourseid), MUST_EXIST);
    if (!$LOCAL_ARCHIVE_INSTANCES[$archiveid]) {
        return false;
    }
    $archive = $LOCAL_ARCHIVE_INSTANCES[$archiveid];
    return $archive->extract_archive_to_dir($acourseid, $filepath, $fileprogress);
}

function local_archive_coursemap_cmp($a, $b) {
    // a total order on coursemaps
    $fields = array('term', 'subject_code', 'catalog_number', 'section_number', 'session_code', 'class_number');
    foreach ($fields as $field) {
        $res = strcmp($a->{$field},$b->{$field});
        if ($res) {
            return $res;
        }
    }
    return 0;
}

function local_archive_user_assign_cmp($a, $b) {
    // a total order on user assignments
    if ($a->auserid < $b->auserid) {
        return -1;
    } else if ($a->auserid > $b->auserid) {
        return 1;
    }
    if ($a->archiverole < $b->archiverole) {
        return -1;
    } else if ($a->archiverole > $b->archiverole) {
        return 1;
    }
    return 0;
}

// Very rough upper bound on restore time in minutes.
function local_archive_estimate_restore_time($importid) {
    global $DB;
    $archivesize = $DB->get_field('archive_course', 'archivesize', array('id' => $importid), MUST_EXIST);
    $threshold = 100000000;  // ~ 100 meg
    if (is_null($archivesize) || $archivesize < $threshold) {
        return 5;
    } else {
        return 15;
    }
}

function local_archive_post_import_tasks($courseid, $withuserdata) {
    global $DB;

    if (!$withuserdata) {
        // Delete all unused section groupings/groups.

       $activegroupings = $DB->get_records_menu('enrol', array('courseid' => $courseid, 'enrol' => 'wisc'), '', 'id,' . enrol_wisc_plugin::groupingidfield);

        // We identify the section groupings based on the default description.  This isn't ideal, but moodle doesn't
        // provide a component field for groupings.

        $groupings = groups_get_all_groupings($courseid);
        foreach ($groupings as $grouping) {
            if (!preg_match('/The groups in this grouping are kept in sync with the class roster/', $grouping->description)) {
                // Doesn't appear to be a section grouping, so ignore.
                continue;
            }
            if (in_array($grouping->id, $activegroupings)) {
                // Active grouping, ignore
                continue;
            }
            $groups = groups_get_all_groups($courseid, 0, $grouping->id);
            foreach ($groups as $group) {
                groups_delete_group($group);
            }
            groups_delete_grouping($grouping);
        }
    }
}

function local_archive_execute_restore($courseid, $importid,
                $restoretarget = backup::TARGET_NEW_COURSE,
                $userid = null,
                $withuserdata = false,
                \core\progress\base $progress = null,
                base_logger $logger = null) {
    global $USER, $DB, $CFG, $OUTPUT;

    if (!$userid) {
        $userid = $USER->id;
    }
    if (!$progress) {
        $progress = new \core\progress\null();
    }
    if (!$logger) {
        $logger = new output_indented_logger();
    }

    $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);

    // Start a progress section for the restore.
    // The max value of 9 is taken from the standard restore.php script.
    $progress->start_progress('Archive import', 9);

    $filepath = restore_controller::get_tempdir_name($courseid, $userid);
    if (!is_dir("$CFG->tempdir/backup")) {
        mkdir("$CFG->tempdir/backup",$CFG->directorypermissions);
    }
    $tempdestination = "$CFG->tempdir/backup/$filepath";
    list($res,$ft) = local_archive_extract_archive_to_dir($importid, $tempdestination);

    // Check whether the backup directory still exists. If missing, something
    // went really wrong in backup, throw error.
    if (!$res || !file_exists($tempdestination) || !is_dir($tempdestination)) {
        print_error('unknownbackupexporterror'); // shouldn't happen ever
    }

    // Note that we've done that progress.
    $progress->progress(1);

    // Prepare the restore controller. We don't need a UI here as we will just import everything
    // except user data.  MODE_IMPORT takes care of the later.
    $rc = new restore_controller($filepath, $course->id, backup::INTERACTIVE_YES, backup::MODE_GENERAL, $userid, $restoretarget);

    $rc->set_progress($progress);

    // Set logger for restore.
    $rc->add_logger($logger);

    // General restore settings
    $restoresettings = array(
            'activities' => 1,
            'blocks' => 1,
            'filters' => 1,
            'users' => $withuserdata? 1: 0,
            'role_assignments' => $withuserdata? 1: 0,
            'calendarevents' => 1,
            'comments' => 1,
            'userscompletion' => $withuserdata? 1: 0,
            'logs' => $withuserdata? 1: 0,
            'grade_histories' => $withuserdata? 1: 0,  // >= 2.6
            'enrol_migratetomanual' => 0,
    );


    // Convert the backup if required....
    if ($rc->get_status() == backup::STATUS_REQUIRE_CONV) {
        // Moodle 1.9 backups are missing many features.  We only restore course activities.
        $restoresettings['filters'] = 0;
        $restoresettings['blocks'] = 0;
        $restoresettings['comments'] = 0;
        $restoresettings['users'] = 0;
        $restoresettings['role_assignments'] = 0;
        $restoresettings['calendarevents'] = 0;
        $restoresettings['userscompletion'] = 0;
        $restoresettings['logs'] = 0;
        $restoresettings['grade_histories'] = 0;
        $rc->convert();
    }
    // Mark the UI finished.
    $rc->finish_ui();

    foreach ($restoresettings as $name => $value) {
        if ($rc->get_plan()->setting_exists($name)) {
            $setting = $rc->get_plan()->get_setting($name);
            if ($setting->get_status() == base_setting::NOT_LOCKED) {
                $setting->set_value($value);
            }
        }
    }

    // Try to save the course startdate.  This is most useful if the user has moodle/restore:rolldates capability, since
    // then the import will roll assignment dates forward
    $setting = $rc->get_plan()->get_setting('course_startdate');
    if ($setting->get_status() === base_setting::NOT_LOCKED) {
        $setting->set_value($course->startdate);
        //$setting->set_status(base_setting::LOCKED_BY_CONFIG);
    }

    // Preserve fullname and shortname
    $rc->get_plan()->get_setting('course_fullname')->set_value($course->fullname);
    $rc->get_plan()->get_setting('course_shortname')->set_value($course->shortname);

    // Execute prechecks
    $warnings = false;
    if (!$rc->execute_precheck()) {
        $precheckresults = $rc->get_precheck_results();
        if (is_array($precheckresults) && !empty($precheckresults['errors'])) {
            fulldelete($tempdestination);

            echo $OUTPUT->header();
            echo html_writer::start_tag('div', array('class'=>'restore-precheck-notices'));
            if (array_key_exists('errors', $precheckresults)) {
                foreach ($precheckresults['errors'] as $error) {
                    echo $OUTPUT->notification($error);
                }
            }
            if (array_key_exists('warnings', $precheckresults)) {
                foreach ($precheckresults['warnings'] as $warning) {
                    echo $OUTPUT->notification($warning, 'notifywarning notifyproblem');
                }
            }
            echo html_writer::end_tag('div');
            echo $OUTPUT->continue_button(new moodle_url('/course/view.php', array('id'=>$course->id)));
            echo $OUTPUT->footer();
            die();
        }
        if (!empty($precheckresults['warnings'])) { // If warnings are found, go ahead but display warnings later.
            $warnings = $precheckresults['warnings'];
        }
    }

    if ($restoretarget == backup::TARGET_CURRENT_DELETING || $restoretarget == backup::TARGET_EXISTING_DELETING) {
        restore_dbops::delete_course_content($course->id);
    }

    // Save the startdate
    $originalstartdate = $course->startdate;

    // Execute the restore
    $rc->execute_plan();
    if ($restoretarget == backup::TARGET_NEW_COURSE) {
        // Restore the startdate.  If the user doesn't have moodle/restore:rolldates capability, then the course startdate
        // will be set the archive startdate.  We don't want that.
        $DB->set_field('course', 'startdate', $originalstartdate, array('id'=>$course->id));
    }

    // Delete the temp directory now
    fulldelete($tempdestination);

    // Do any post import tasks.
    local_archive_post_import_tasks($courseid, $withuserdata);

    // End restore section of progress tracking (restore/precheck).
    $progress->end_progress();

    // Display warning notifications
    if ($warnings) {
        echo $OUTPUT->box_start();
        echo $OUTPUT->notification(get_string('warning'), 'notifyproblem');
        echo html_writer::start_tag('ul', array('class'=>'list'));
        foreach ($warnings as $warning) {
            echo html_writer::tag('li', $warning);
        }
        echo html_writer::end_tag('ul');
        echo $OUTPUT->box_end();
    }



}
