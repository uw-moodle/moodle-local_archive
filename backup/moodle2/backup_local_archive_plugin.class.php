<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Support for backup of category structure.
 * This class ammends course backups with all parent course category data.  This
 * allows us to grant category managers access to archives from courses in their own
 * categories.
 *
 * @package    local_archive
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Defines archive backup structures
 *
 * TODO: This class should derive from backup_local_plugin, but that class is only present in moodle 2.4
 */
class backup_local_archive_plugin extends backup_plugin {

    /**
     * Attach local_archive structure to the $course element
     *
     * @return backup_plugin_element
     */
    public function define_course_plugin_structure() {
        global $DB;

        // Define the virtual plugin element
        $plugin = $this->get_plugin_element(null);

        // Create one standard named plugin element (the visible container)
        $pluginwrapper = new backup_nested_element($this->get_recommended_name());

        // Connect the visible container ASAP
        $plugin->add_child($pluginwrapper);

        // Define the parentcategory container
        $categories = new backup_nested_element('parentcategories');

        // Connect the categories container
        $pluginwrapper->add_child($categories);

        // Now create the category structures
        $category = new backup_nested_element('parentcategory', array('id'), array(
                'name', 'description', 'idnumber'));

        // Fetch all parent categories
        $catid = $DB->get_field('course', 'category', array('id' => $this->task->get_courseid()));
        $cats = array();
        while ($catid) {
            $cat = $DB->get_record('course_categories', array('id' => $catid));
            if (!$cat) {
                // missing category?
                break;
            }
            if (isset($cats[$cat->id])) {
                // prevent an infinite loop
                break;
            }
            $cats[$cat->id] = $cat;
            $catid = $cat->parent;
        }

        $category->set_source_array($cats);
        $categories->add_child($category);

        return $plugin;

    }
}
