<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Convert essaywithstart questions to essay questions in moodle >= 2.5.
 * The essaywithstart question type has been obsoleted now that essay questions
 * have a response template.  This code handles conversion during course restore.
 *
 * @package    local_archive
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


class restore_local_archive_plugin extends restore_qtype_plugin {

    var $processing_essaywithstart = false; // Are we handling conversion?

    /**
     * Returns the paths to be handled by the plugin at question level
     */
    protected function define_question_plugin_structure() {
        global $CFG;

        $structure = array();
        // Convert essaywithstart to essay if essaywithstart isn't installed and moodle >= 2.5
        // Moodle 2.5 essay question type includes a response template, which obsoletes the essaywithstart question type.
        if (!class_exists('restore_qtype_essaywithstart_plugin') && $CFG->version >= 2013051400) {
            $this->processing_essaywithstart = true;
            $structure[] = new restore_path_element('essaywithstart', '/question_categories/question_category/questions/question/plugin_qtype_essaywithstart_question/essaywithstart');
        }
        return $structure;
    }

    /**
     * Process the qtype/essaywithstart element and convert to qtype/essay
     *
     * This function creates the qtype_essay_options record, but by this point the question record has already
     * been created with type essaywithstart.  The function after_execute_question finishes the job
     */
    public function process_essaywithstart($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        // convert options from essaywithstart to essay
        $data->responseformat = 'editor';
        $data->responsefieldlines = 15;
        $data->attachments = 0;
        $data->graderinfo = '';
        $data->graderinfoformat = FORMAT_HTML;
        $data->responsetemplate = $data->start;
        $data->responsetemplateformat = FORMAT_HTML;

        // Detect if the question is created or mapped.
        $questioncreated = $this->get_mappingid('question_created',
                $this->get_old_parentid('question')) ? true : false;

        // If the question has been created by restore, we need to create its
        // qtype_essay too.
        if ($questioncreated) {
            $data->questionid = $this->get_new_parentid('question');

            $newitemid = $DB->insert_record('qtype_essay_options', $data);
            $this->set_mapping('qtype_essay', $oldid, $newitemid);
        }
    }

    /**
     * Return the contents of this qtype to be processed by the links decoder
     */
    public static function define_decode_contents() {
        return array();
    }

    /**
     * Find essaywithstart qtypes which we restored, and convert to qtype essay
     */
    protected function after_execute_question() {
        global $DB, $DBM;

        if ($this->processing_essaywithstart) {
            $essaywithstarts = $DB->get_records_sql("
                        SELECT *
                          FROM {question} q
                         WHERE q.qtype = ?
                           AND EXISTS (
                            SELECT 1
                              FROM {qtype_essay_options}
                             WHERE questionid = q.id
                         )
                    ", array('essaywithstart'));

            foreach ($essaywithstarts as $q) {
                $DB->set_field('question', 'qtype', 'essay', array('id'=>$q->id));
            }
        }
    }
}
