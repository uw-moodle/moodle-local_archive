<?php

require_once($CFG->dirroot.'/local/archive/subjects.php');
require_once($CFG->dirroot.'/backup/util/ui/renderer.php');

if (file_exists($CFG->dirroot.'/enrol/wisc/lib/datastore.php')) {
    require_once($CFG->dirroot.'/enrol/wisc/lib/datastore.php');
}

class local_archive_renderer extends core_backup_renderer {
    /**
     * Renders an archive course search object
     *
     * @param import_course_search $component
     * @return string
     */
    public function render_archive_course_search(archive_course_search $component, $selected = null) {
        global $CFG;
        $perpage = $component->get_perpage();
        $page = $component->get_page();

        $url = $component->get_url();

        $count = $component->count();
        $courses = $component->search($page*$perpage, $perpage);

        $showadvanced = $component->get_showadvanced();
        $canshowadvanced = $component->can_show_advanced();

        // Get subject mapping
        $subjects = array();
        // from configured mapping, if set
        if (isset($CFG->local_archive_departments)) {
            $subjects = $CFG->local_archive_departments;
        }
        // then from datastore
        if (class_exists('wisc_timetable_datastore')) {
            try {
                $datastore = new \enrol_wisc\local\chub\chub_datasource();
                $terms = $datastore->getAvailableTerms();
                $aterm = reset($terms);
                $ttsubjects = $datastore->getSubjectsInTerm($aterm->termCode);
                foreach ($ttsubjects as $subject) {
                    if (!isset($subjects[$subject->subjectCode])) {
                        $subjects[$subject->subjectCode] = str_replace(' ', '', $subject->shortDescription);
                    }
                }
            } catch (Exception $e) {
                error_log('Error reading subjects from datastore');
            }
        }

        $output = '';
        // Add paging params
        $output .= html_writer::empty_tag('input', array('type'=>'hidden', 'name'=>'page', 'value'=>$component->get_page()));
        $output .= html_writer::empty_tag('input', array('type'=>'hidden', 'name'=>'perpage', 'value'=>$component->get_perpage()));
        if ($component->get_showadvanced()) {
            $output .= html_writer::empty_tag('input', array('type'=>'hidden', 'name'=>'advanced', 'value'=>1));
        }
        if ($count == 0) {
            $output .= html_writer::start_tag('div');
            $output .= html_writer::tag('h2', get_string('nomatches', 'local_archive'));
            $output .= html_writer::end_tag('div');
        } else {
            /* Show search results breakdown */
            $output .= html_writer::start_tag('div');
            $output .= html_writer::tag('p', get_string('coursecount', 'local_archive', $count));
            $output .= html_writer::end_tag('div');
            $output .= $this->output->paging_bar($count, $page, $perpage, $url);

            /* Display archive selection table */
            $output .= html_writer::start_tag('div', array('class' => 'ics-results'));
            $table = new html_table();
            if (!$canshowadvanced) {
                $table->head = array('', 'Term', 'Catalog', get_string('fullnamecourse'), 'Archived', 'Size');
            } else {
                $table->head = array('', 'Term', 'Catalog', get_string('fullnamecourse'), 'Type', 'Filename', 'Archived', 'Size', 'Teachers');
            }
            $table->data = array();
            foreach ($courses as $course) {
                $row = new html_table_row();
                $row->attributes['class'] = 'ics-course';
                // Convert termcode to string
                if ($course->term) {
                    $term = wisc_timetable_datastore::get_term_name($course->term);
                } else {
                    $term = "";
                }
                // Convert department numbers to names.
                if (!empty($course->catalog)) {
                    $catalog = "";
                    $entries = explode(',', $course->catalog);
                    foreach ($entries as &$entry) {
                        list($dept,$number) = explode('.', $entry);
                        if (isset($subjects[$dept])) {
                            $dept = $subjects[$dept];
                        } else {
                            $dept = '('.$dept.')';
                        }
                        $entry = "$dept $number";
                    }
                    $catalog = implode(',',$entries);
                } else {
                    $catalog = "";
                }
                $archivedate = userdate($course->archivedate, '%m/%d/%y');
                $archivesize = display_size($course->archivesize);
                $inputparams = array('type'=>'radio', 'name'=>'importid', 'value'=>$course->id);
                if ($course->id === $selected) {
                    $inputparams['checked'] = 'checked';
                }
                if (!$canshowadvanced) {
                    $row->cells = array(
                        html_writer::empty_tag('input', $inputparams),
                        $term,
                        $catalog,
                        s($course->coursename),
                        $archivedate,
                        $archivesize,
                    );
                } else {
                    $row->cells = array(
                        html_writer::empty_tag('input', $inputparams),
                        $term,
                        $catalog,
                        s($course->coursename),
                        $course->type,
                        $course->data,
                        $archivedate,
                        $archivesize,
                        $course->teachers,
                    );
                }
                $table->data[] = $row;
            }
            $output .= html_writer::table($table);
            $output .= $this->output->paging_bar($count, $page, $perpage, $url);
            $output .= html_writer::end_tag('div');
        }
        $output .= $this->import_search($component, $subjects);
        return $output;
    }

    /**
     * Displays the import course selector
     *
     * @param moodle_url $nextstageurl
     * @param import_course_search $courses
     * @return string
     */
    public function archive_import_course_selector(archive_course_search $courses, moodle_url $url) {
        $html  = html_writer::start_tag('div', array('class'=>'import-course-selector backup-restore'));
        $html .= $this->output->heading(get_string('importdatafrom'), 2, array('class'=>'header'));
        $html .= html_writer::start_tag('form', array('method'=>'post', 'action'=>$url));
        $html .= $this->render($courses);

        $html .= html_writer::start_tag ( 'div', array ('class' => 'form-buttons') );
        $html .= html_writer::empty_tag ( 'input', array ('type' => 'submit', 'value' => get_string ( 'next' ), 'name' => 'import', 'class'=>'nextbutton form-submit' ) );
        $html .= html_writer::empty_tag ( 'input', array ('type' => 'submit', 'value' => get_string ( 'cancel' ), 'name' => 'cancel', 'class' => 'confirmcancel cancelbutton' ) );
        $html .= html_writer::end_tag ( 'div' );

        $html .= html_writer::end_tag('form');
        $html .= html_writer::end_tag('div');
        return $html;
    }

    public function backup_details_confirm(moodle_url $url, array $params, array $details) {

        $html  = html_writer::start_tag('div', array('class' => 'backup-restore nonstandardformat'));
        $html .= html_writer::start_tag('form', array('method'=>'post', 'action'=>$url));
        $html .= $this->helper_output_params($params);
        $html .= html_writer::start_tag('div', array('class' => 'backup-section'));
        $html .= $this->output->heading('Import details', 2, 'header');
        $html .= $this->backup_detail_pair('Archive name', s($details['coursename']));
        $html .= $this->backup_detail_pair('Archive date', userdate($details['archivedate']));
        $html .= $this->backup_detail_pair('Archive size', display_size($details['archivesize']));
        $html .= html_writer::end_tag('div');
        // Restore options
        $html .= html_writer::start_tag('div', array('class' => 'bcs-current-course backup-section'));
        $html .= $this->output->heading('Import into this course', 2, array('class' => 'header'));
        $html .= $this->backup_detail_input('Merge the archive into this course', 'radio', 'target',
                backup::TARGET_CURRENT_ADDING, array('checked' => 'checked'));
        $html .= $this->backup_detail_input('Delete the contents of this course and then import', 'radio', 'target',
                backup::TARGET_CURRENT_DELETING);
        $html .= html_writer::end_tag('div');

        if ($details['canrestoreusers']) {
            $html .= html_writer::start_tag('div', array('class' => 'bcs-current-course backup-section'));
            $html .= $this->output->heading('Advanced', 2, array('class' => 'header'));
            $html .= $this->backup_detail_input('Restore with user data', 'checkbox', 'enableuserinfo', 1, array(), '<em> (Only recommended for backups from the same site.)</em>');
            $html .= html_writer::end_tag('div');
        }

        $html .= html_writer::start_tag ( 'div', array ('class' => 'form-buttons') );
        $html .= html_writer::empty_tag ( 'input', array ('type' => 'submit', 'value' => get_string ( 'previous' ), 'name' => 'previous', 'class'=>'previousbutton' ) );
        $html .= html_writer::empty_tag ( 'input', array ('type' => 'submit', 'value' => 'Import entire archive', 'name' => 'importfull', 'class'=>'nextbutton form-submit' ) );
        $html .= html_writer::empty_tag ( 'input', array ('type' => 'submit', 'value' => 'Customize import', 'name' => 'importcustom', 'class'=>'nextbutton form-submit' ) );
        $html .= html_writer::empty_tag ( 'input', array ('type' => 'submit', 'value' => get_string ( 'cancel' ), 'name' => 'cancel', 'class' => 'confirmcancel cancelbutton' ) );
        $html .= html_writer::end_tag ( 'div' );
        $html .= html_writer::end_tag('form');
        $html .= html_writer::end_tag('div');

        return $html;
    }

    protected function helper_output_params($params) {
        $output = html_writer::empty_tag ( 'input', array ('type' => 'hidden', 'name' => 'sesskey', 'value' => s ( sesskey () ) ) );
        if (is_array ( $params ) && count ( $params ) > 0) {
            foreach ( $params as $name => $value ) {
                $output .= html_writer::empty_tag ( 'input', array ('type' => 'hidden', 'name' => $name, 'value' => $value ) );
            }
        }
        return $output;
    }

    public function import_search(archive_course_search $component, $subjects) {

        $canshowadvanced = $component->can_show_advanced();
        $showadvanced = $component->get_showadvanced();
        $output = '';
        $output .= html_writer::tag('h4', 'Search');
        $output .= $this->output->box_start('generalbox');
        $output .= html_writer::start_tag('div', array('class' => 'import-course-search mform'));

        $output .= $this->add_filter($component, 'course' , 30);

        asort($subjects);
        $output .= $this->add_filter_select($component, 'subject', $subjects, ' ');
        $output .= $this->add_filter($component, 'catalog',3);
        if($showadvanced){
            $output .= $this->add_filter($component, 'user',15);
            $output .= $this->add_filter($component, 'data',15);
        }
        $output .= html_writer::start_tag('div', array('class'=>'fitem'));
        $output .= html_writer::start_tag('div', array('class'=>'felement'));
        $output .= html_writer::empty_tag('input', array('type'=>'submit', 'name'=>'searchcourses', 'value'=>get_string('search')));
        $output .= html_writer::end_tag('div');
        $output .= html_writer::end_tag('div');

        $output .= html_writer::end_tag('div');
        $output .= $this->output->box_end();
        return $output;
    }

    protected function add_filter(archive_course_search $search, $name, $length){
        $label = get_string('search'.$name,'local_archive');
        $tagname = archive_course_search_base::$VAR_SEARCH.'_'.$name;
        $attr = array('type'=>'text', 'size'=>$length,'name'=>$tagname, 'value'=>$search->get_search($name));
        $output = '';
        $output .= html_writer::start_tag('div', array('class'=>'fitem'));
        $output .= html_writer::start_tag('div', array('class'=>'fitemtitle'));
        $output .= $label;
        $output .= html_writer::end_tag('div');
        $output .= html_writer::start_tag('div', array('class'=>'felement'));
        $output .= html_writer::empty_tag('input', $attr);
        $output .= html_writer::end_tag('div');
        $output .= html_writer::end_tag('div');
        return $output;
    }

    protected function add_filter_select(archive_course_search $search, $name, array $options, $nothing){
        $label = get_string('search'.$name,'local_archive');
        $tagname = archive_course_search_base::$VAR_SEARCH.'_'.$name;
        $selected = $search->get_search($name);
        $options = array_map('s', $options);
        $output = '';
        $output .= html_writer::start_tag('div', array('class'=>'fitem'));
        $output .= html_writer::start_tag('div', array('class'=>'fitemtitle'));
        $output .= $label;
        $output .= html_writer::end_tag('div');
        $output .= html_writer::start_tag('div', array('class'=>'felement'));
        $output .= html_writer::select($options, $tagname,$selected,$nothing);
        $output .= html_writer::end_tag('div');
        $output .= html_writer::end_tag('div');
        return $output;
    }
}
