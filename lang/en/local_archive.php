<?php
$string['pluginname'] = 'WISC archive plugin';
$string['pluginname_desc'] = '<p>This plugin enables importing of course archives stored in server directories.</p>';

$string['archive:anyarchive'] = 'Import any archive';
$string['archive:dolocalimport'] = 'Perform native moodle import';
$string['searchcourse'] = 'Course full name';
$string['searchsubject'] = 'Department';
$string['searchcatalog'] = 'Catalog number';
$string['searchuser'] = 'Archive user';
$string['searchdata'] = 'Archive filename';

$string['nomatches'] = 'No archives found';
$string['coursecount'] = 'Courses: {$a}';

$string['rebuildindex'] = 'Rescan archives';
$string['scanbutton'] = 'Scan';
$string['selectarchives'] = 'Select which archives to scan';

$string['default_moodle_import_link_text'] = 'Import (current site)';
$string['import_link_text'] = 'Import from archives';


// Settings string
$string['settings_page_name'] = 'WISC archive plugin';
$string['override_link_setting'] = 'Override default import url';
$string['override_link_setting_desc'] = '<p>This setting will change the default import link in course administration to the WISC archive ' .
                                        'import functionality instead the default.</p><p>For users with the local/archive:dolocalimport capability, both links will be displayed.</p>';