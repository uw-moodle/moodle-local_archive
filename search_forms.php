<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/lib/formslib.php');

class archive_search_form extends moodleform {
    protected $params;

    function __construct($params, $action=null, $customdata=null, $method='post', $target='', $attributes=null, $editable=true) {
        $this->params = $params;
        parent::__construct($action, $customdata, $method, $target, $attributes, $editable);
    }

    function definition() {
        global $CFG;

        $mform       =& $this->_form;

        $params = $this->params;
        if (is_array($params) && count($params) > 0) {
            foreach ($params as $name=>$value) {
                $stage = $mform->addElement('hidden', $name, $value);
            }
        }

        $mform->addElement('header','searchbox', get_string('search'));

        $mform->addElement('text','course', get_string('fullnamecourse'));
        $mform->setType('course', PARAM_TEXT);

        $subjects = $CFG->local_archive_departments;
        asort($subjects);
        array_unshift($subjects, '');
        $mform->addElement('select', 'subject', get_string('subject', 'local_archive'), $subjects);
        $mform->setAdvanced('subject');

        $mform->addElement('text','catalog', get_string('catalog', 'local_archive'));
        $mform->setType('catalog', PARAM_ALPHANUMEXT);
        $mform->setAdvanced('catalog');

        $mform->addElement('text','user', get_string('user', 'local_archive'));
        $mform->setType('user', PARAM_ALPHANUMEXT);
        $mform->setAdvanced('user');

        $mform->addElement('text','filename', get_string('filename', 'local_archive'));
        $mform->setType('filename', PARAM_ALPHANUMEXT);
        $mform->setAdvanced('filename');

        // Search button
        $mform->addElement('submit', 'search', get_string('search'));
    }
}
