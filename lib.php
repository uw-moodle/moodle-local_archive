<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * lib file
 * Is included on every page load with config.php
 *
 * @package    local
 * @subpackage archive
 * @copyright  2014 University of Wisconsin - Madison
 * @author     John Hoopes <hoopes@wisc.edu>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


/**
 * Replace existing import url and add the local one if user has site:config
 *
 * @param global_navigation $settingsnav Navigation object
 * @param object $context The context running under
 *
 */
function local_archive_extend_settings_navigation($settingsnav, $context){
    global $PAGE;

    $enabled = get_config('local_archive', 'enable_menu_override');

    // Don't modify anything if the configuration isn't enabled
    if( (int)$enabled !== 1 ){
        return;
    }

    if($context->contextlevel == CONTEXT_SYSTEM || empty($context)){
        return;
    }

    $coursecontext = $context->get_course_context(false);

    // If there is no course context return we aren't in a course
    if(empty($coursecontext)){
        return;
    }

    // Change import url
    if(has_capability('moodle/restore:restoretargetimport', $context)){
        if($courseadmin = $settingsnav->find('courseadmin', navigation_node::TYPE_COURSE)) {
            if($importnode = $courseadmin->get('import', navigation_node::TYPE_SETTING) ){
                // replace default import action url
                $importnode->text = get_string('import_link_text', 'local_archive');
                $importnode->action = new moodle_url('/local/archive/import.php', array('id'=>$coursecontext->instanceid));
            }
        }
    }

    // Add Moodle local import for site admins
    if(has_capability('local/archive:dolocalimport', $coursecontext)){
        if($courseadmin = $settingsnav->find('courseadmin', navigation_node::TYPE_COURSE)){

            // add in a link to the default Moodle import for site admins
            $linktext = get_string('default_moodle_import_link_text', 'local_archive');
            $url = new moodle_url('/backup/import.php', array('id'=>$coursecontext->instanceid));
            $defaultimport = navigation_node::create(
                $linktext,
                $url,
                navigation_node::TYPE_SETTING,
                'default_import',
                'local_archive_default_import',
                new pix_icon('i/import', $linktext)
            );
            if($PAGE->url->compare($url, URL_MATCH_BASE)) {
                $defaultimport->make_active();
            }
            $courseadmin->add_node($defaultimport, 'publish');
        }
    }
}



