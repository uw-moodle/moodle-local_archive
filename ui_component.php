<?php

require_once($CFG->dirroot . '/local/archive/locallib.php');
require_once($CFG->dirroot . '/backup/util/ui/restore_ui_components.php');


/**
 * archive search component
 */
abstract class archive_course_search_base implements renderable {

    static $VAR_SEARCH = 'search';

    /**
     * The current search strings
     * @var array|empty
     */
    private $search = array();

    /**
     * Required params to return to this page
     */
    protected $pageparams = array();

    /**
     * All search parameters
     */
    protected $state = array();

    /**
     * @param array $config
     */
    public function __construct(array $config=array()) {
        $this->init_state();

        foreach ($config as $name=>$value) {
            $method = 'set_'.$name;
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    public function get_searchfields() {
        return array();
    }

    public function can_show_advanced() {
        $systemcontext = context_system::instance();
        return has_capability('local/archive:anyarchive', $systemcontext);
    }

    public function set_state($state) {
        $this->state = array_merge((array) $state, $this->state); // previous state overrides
    }

    public function get_state() {
        return $this->state;
    }

    public function get_perpage() {
        if (!empty($this->state['perpage'])) {
            return $this->state['perpage'];
        } else {
            return 10;
        }
    }

    public function get_page() {
        if (!empty($this->state['page'])) {
            return $this->state['page'];
        } else {
            return 0;
        }
    }

    public function get_showadvanced() {
        return $this->can_show_advanced();
    }

    /**
     * The URL for this search
     * @global moodle_page $PAGE
     * @return moodle_url The URL for this page
     */
    final public function get_url() {
        global $PAGE;

        $params = $this->pageparams;
        $searchfields = $this->get_searchfields();
        foreach($searchfields as $field){
            $search = $this->get_search($field);
            if($search != ''){
                $params[self::$VAR_SEARCH.'_'.$field] = $search;
            }
        }
        return new moodle_url($PAGE->url, $params);
    }
    /**
     * The current search string
     * @return string
     */
    final public function get_search($field) {
        return isset($this->state[$field]) ? $this->state[$field] : '';
    }

    final public function set_pageparams(array $pageparams) {
        $this->pageparams = $pageparams;
    }

    /**
     * Counts the total number of matches
     *
     * @return int the count
     */
    final public function count() {
        global $DB;
        list($sql, $params) = $this->get_searchsql();
        if (!$sql) {
            return 0;
        }
        // this could probably be a little more efficient, but the sql uses GROUP BY
        // which forces us to use a subquery anyhow
        $countsql = "SELECT COUNT(*) FROM ( $sql ) c";
        return $DB->count_records_sql($countsql, $params);
    }

    /**
     * Executes the search
     *
     * @return array The results
     */
    final public function search($start = 0, $count = 0) {
        global $DB;

        list($sql, $params) = $this->get_searchsql();
        if (!$sql) {
            return array();
        }

        $results = $DB->get_records_sql($sql, $params, $start, $count);

        return $results;
    }

    abstract protected function get_searchsql();

    protected function get_searchfieldsql($field, $sqlfield){
        global $DB;

        $search = $this->get_search($field);
        if(empty($search)){
            return array('', array());
        }

        /* Search for phrases and if not break on spaces */
        if(preg_match_all('/"([^"]+)"/',$search, $phrases)){
            $phrases = $phrases[1];
        }else{
            $phrases = explode(' ',$search);
        }

        $i = 0;
        foreach($phrases as $phrase){
            $i++;
            $params[$field.'search'.$i] = '%'.$phrase.'%';
            $sql[] = $DB->sql_like($sqlfield,':'.$field.'search'.$i,false);
        }

        return array(implode(" AND ",$sql),$params);
    }

    protected function init_state() {
        $searchfields = $this->get_searchfields();
        foreach($searchfields as $field){
            $value = optional_param(self::$VAR_SEARCH.'_'.$field, null, PARAM_TEXT);
            if (!is_null($value)) {
                $this->state[$field] = $value;
            }
        }
        // page parameters
        $perpage = optional_param('perpage', null, PARAM_INT);
        if (!is_null($perpage)) {
            $this->state['perpage'] = $perpage;
        }
        $page = optional_param('page', null, PARAM_INT);
        if (!is_null($page)) {
            $this->state['page'] = $page;
        }
        $newsearch = !is_null(optional_param('searchcourses', null, PARAM_TEXT));
        if ($newsearch) {
            $this->state['page'] = 0; // search resets paging
        }

    }
}

/**
 * archive search component
 */
class archive_course_search extends archive_course_search_base implements renderable {

    public function get_searchfields() {
        return array('course','subject','catalog','data','user');
    }

    protected function get_searchsql() {
        global $DB;
        global $USER;
        global $LOCAL_ARCHIVE_INSTANCES;

        $systemcontext = context_system::instance();
        $anyarchive = has_capability('local/archive:anyarchive', $systemcontext);

        $searchsql = array();
        $params = array();

        // USER WHERE clause
        if (!$anyarchive) {
            $ausers = array_keys(local_archive_find_matching_users($USER));
            if (!$ausers) {
                return false; // no matching archive users
            }
            list($usql, $uparams) = $DB->get_in_or_equal($ausers, SQL_PARAMS_NAMED);
            $searchsql[] = "c.id in (SELECT aa.acourseid FROM {archive_user_assign} aa
                                 WHERE aa.auserid $usql)";
            $params = array_merge($params, $uparams);
        }

        // ARCHIVE ID WHERE clause
        local_archive_init_archives();
        $archiveids = array_keys($LOCAL_ARCHIVE_INSTANCES);
        if (!$archiveids) {
            return false;
        }
        list($asql,$aparams) = $DB->get_in_or_equal($archiveids, SQL_PARAMS_NAMED);
        $searchsql[] = "v.id $asql";
        $params = array_merge($params, $aparams);

        // user search
        $user = $this->get_search('user');
        if (!empty($user) && $this->can_show_advanced()) {
            $params['netid'] = $user;
            $usersql[] = $DB->sql_like('u.netid',':netid',false);
            $params['firstname'] = '%'.$user.'%';
            $usersql[] = $DB->sql_like('u.firstname',':firstname',false);
            $params['lastname'] = '%'.$user.'%';
            $usersql[] = $DB->sql_like('u.lastname',':lastname',false);
            $params['externalid'] = $user;
            $usersql[] = $DB->sql_like('u.externalid',':externalid',false);
            $searchsql[] = "(".implode(' OR ', $usersql).")";
        }

        // coursename search
        list($ssql, $sparams) = $this->get_searchfieldsql('course', 'c.coursename');
        if (!empty($ssql)) {
            $searchsql[] = $ssql;
            $params = array_merge($params, $sparams);
        }

        // catalog search
        $catalog = $this->get_search('catalog');
        if ($catalog) {
            $searchsql[] = 'm.catalog_number = :catalog_number';
            $params['catalog_number'] = $catalog;
        }
        $subject = $this->get_search('subject');
        if ($subject) {
            $searchsql[] = 'm.subject_code = :subject_code';
            $params['subject_code'] = $subject;
        }

        // data search
        $data = $this->get_search('data');
        if ($data && $this->can_show_advanced()) {
            $searchsql[] = 'c.data = :data';
            $params['data'] = $data;
        }

        $searchsql = implode(' AND ', $searchsql);

        $sql = "SELECT c.id,
                       c.coursename,
                       c.archivedate,
                       c.archivesize,
                       v.name as type,
                       c.data,
                       m.term,
                       GROUP_CONCAT(DISTINCT CONCAT(m.subject_code,'.',m.catalog_number)) as catalog,
                       GROUP_CONCAT(DISTINCT u.netid) as teachers,
                       GROUP_CONCAT(CONCAT_WS(' ', u.firstname,u.lastname,u.externalid,u.netid)) as usersearch
                  FROM mdl_archive_course c
                  LEFT JOIN mdl_archive_coursemap m
                      ON m.acourseid = c.id
                  LEFT JOIN mdl_archive_user_assign a
                      ON a.acourseid = c.id
                  LEFT JOIN mdl_archive_user u
                      ON a.auserid = u.id
                  JOIN mdl_archive v
                      ON v.id = c.archiveid
                  WHERE
                      $searchsql
                  GROUP BY c.id
                  ORDER BY term DESC, coursename ASC";

        return array($sql, $params);
    }
}
