<?php
defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/filestorage/zip_packer.php");

class archive_zip_packer extends zip_packer{
    /**
     * Extract a single file to given directory (real OS filesystem), existing files are overwritten
     * @param mixed $archivefile full pathname of zip file or stored_file instance
     * @param string $directory target directory
     * @param string $file name of the file we want to extract
     * @return mixed true on success; error string otherwise
     */
    public function extract_file_to_directory($archivefile, $directory, $file) {
        global $CFG;

        if (!is_string($archivefile)) {
            return $archivefile->extract_file_to_pathname($this, $directory);
        }

        $files = explode(',',$file);
        $multiple = false;
        if(($wildcard = strstr($file,'*')) || count($files) > 1){
            $multiple = true;
        }

        $directory = rtrim($directory, '/');
        if (!is_readable($archivefile)) {
            throw new moodle_exception('Archive is not readable.');
        }

       $ziparch = new zip_archive();
        if (!$ziparch->open($archivefile, file_archive::OPEN)) {
        	throw new moodle_exception('Failed to open archive.');
        }

        $result = false;
        foreach ($ziparch as $info) {
            $pathname = $info->pathname;
            // Skip file(s) we're looking for
        	if($multiple ? ($wildcard ? !fnmatch($file,$pathname) : !in_array($pathname,$files)) : $pathname != $file){
		        continue;
        	}

            if ($info->is_directory) {
            	$ziparch->close();
            	throw new moodle_exception('Extracting directories is not supported.');
            }

        	$size = $info->size;
            $name = $info->pathname;

            $parts = explode('/', trim($name, '/'));
            $filename = array_pop($parts);
            $newdir = rtrim($directory.'/'.implode('/', $parts), '/');

            if (!is_dir($newdir)) {
                if (!mkdir($newdir, $CFG->directorypermissions, true)) {
                	$ziparch->close();
                	throw new moodle_exception("Cannot create directory $newdir");
                }
            }

            $newfile = "$newdir/$filename";
            if (!$fp = fopen($newfile, 'wb')) {
            	$ziparch->close();
            	throw new moodle_exception("Cannot write target file $newfile");
            }

            if (!$fz = $ziparch->get_stream($info->index)) {
                fclose($fp);
                $ziparch->close();
                throw new moodle_exception("Cannot read file $info->pathname from zip archive");
            }

            while (!feof($fz)) {
                $content = fread($fz, 262143);
                fwrite($fp, $content);
            }
            fclose($fz);
            fclose($fp);

            if (filesize($newfile) !== $size) {
                // something went wrong :-(
                @unlink($newfile);
                $ziparch->close();
                return false;
            }
            if($multiple){
                $result = true;
            }else{
                $ziparch->close();
                return true;
            }
        }
        $ziparch->close();
        return $result;    //Target file(s) not found in archive
    }
}