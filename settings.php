<?php

/**
 * Add menu items
 */

defined('MOODLE_INTERNAL') || die();

global $PAGE;

$modname = 'local_archive';
$plugin = 'local/archive';

if ($hassiteconfig) {
    $scan = new admin_externalpage('scanarchives', 'Rescan archives', "$CFG->wwwroot/local/archive/scan.php", array('moodle/site:config'));
    $ADMIN->add('localplugins', $scan);

    // Add configuration for making user suspension optional
    $settings = new admin_settingpage('local_archive_settings', get_string('settings_page_name', 'local_archive'));

    $settings->add(new admin_setting_heading('local_archive_settings', '',
        get_string('settings_page_name', 'local_archive')));

    $settings->add(new admin_setting_configcheckbox('local_archive/enable_menu_override', get_string('override_link_setting', 'local_archive'),
        get_string('override_link_setting_desc', 'local_archive'), 1));

    $ADMIN->add('localplugins', $settings);

}