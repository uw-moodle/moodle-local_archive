<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2014110400;
$plugin->release = '2.3';
$plugin->requires  = 2014051200;   // See http://docs.moodle.org/dev/Moodle_Versions
$plugin->component = 'local_archive';
$plugin->maturity  = MATURITY_BETA;

$plugin->dependencies = array(
        'enrol_wisc' => 2012071600,
);
