<?php

defined('MOODLE_INTERNAL') || die();

class archive_storagetype_moodle1 extends archive_storagetype_moodle {

    protected $dir;

    public function get_storagetype() {
        return 'backup19';
    }

    protected function get_backup_mtime($temppath) {
        if (file_exists("$temppath/moodle.xml")) {
            $stat = stat("$temppath/moodle.xml");
        } else if (file_exists("$temppath/moodle.xml.bz2")) {
            $stat = stat("$temppath/moodle.xml.bz2");
        } else {
            $stat = false;
        }
        if ($stat) {
            return $stat['mtime'];
        }
        return false;
    }

    protected function extract($archivepath,$temppath) {
        $zp = new archive_zip_packer();

        if(!$zp->extract_file_to_directory($archivepath,$temppath,'moodle.xml')){
            if(!$zp->extract_file_to_directory($archivepath,$temppath,'moodle.xml.bz2')){
                throw new moodle_exception("Failed to extract archive $archivepath");
            }
        }

        return true;
    }
    protected function process_backup($temppath, $archivename, $archivesize) {
        $moodlexml = "$temppath/moodle.xml";
        $simple = false;
        if (file_exists($moodlexml.'.bz2')) {
            $simple = $this->simplexml_load_bzfile($moodlexml.'.bz2');
            if (!$simple) {
                throw new moodle_exception("Can't load  $moodlexml.bz2");
            }
        } else if (file_exists($moodlexml)) {
            $simple = simplexml_load_file( $moodlexml );
            if (!$simple) {
                throw new moodle_exception("Can't load  $moodlexml");
            }
        } else {
            throw new moodle_exception("Can't load  $moodlexml");
        }
        return $this->process_moodlexml($simple, $archivename, $archivesize);
    }

    protected function process_moodlexml($simple, $archivename, $archivesize) {
        global $DB;

        $archive = new stdClass();
        $archive->archiveid = $this->id;
        $archive->data = $archivename;
        $archive->coursename = (string) $simple->COURSE->HEADER->FULLNAME;
        $archive->archivedate = (string) $simple->INFO->DATE;
        $archive->archivesize = $archivesize;
        $archive->lastscan = time();

        // coursemaps
        $coursemaps = array();
        if ($simple->COURSE->UWSECTIONS) {
            foreach ($simple->COURSE->UWSECTIONS->UWSECTION as $section) {
                $coursemap = new stdClass();
                $coursemap->term = (string) $section->TERM;
                $coursemap->subject_code = (string) $section->SUBJECT_CODE;
                $coursemap->catalog_number = (string) $section->CATALOG_NUMBER;
                $coursemap->section_number = (string) $section->SECTION_NUMBER;
                $coursemap->session_code = (string) $section->SESSION_CODE;
                $lmsid = (string) $section->LMSID;  // lmsid = "term_classnumber"
                $parts = explode('_', $lmsid);
                $coursemap->class_number = $parts[1];
                $coursemaps[] = $coursemap;
            }
        }

        //roles
        $userids = array();
        $teacherroles = array_flip($this->roles);
        foreach ($simple->COURSE->HEADER->ROLES_ASSIGNMENTS->ROLE as $role) {
            if (isset($teacherroles[(string)$role->SHORTNAME])) {
                foreach ($role->ASSIGNMENTS->ASSIGNMENT as $assignment) {
                    $userids[(string)$assignment->USERID] = true;
                }
            }
        }

        //users
        $roles = array();
        if ($simple->COURSE->USERS->USER) {
            foreach ($simple->COURSE->USERS->USER as $courseuser) {
                if (!empty($userids[(string)$courseuser->ID])) {
                    $user = new stdClass();
                    if ($courseuser->USER_UWDATA) {
                        $user->pvi = (string) $courseuser->USER_UWDATA->PVI;
                        $user->netid = (string) $courseuser->USER_UWDATA->NETID;
                    }
                    $user->firstname = (string) $courseuser->FIRSTNAME;
                    $user->lastname = (string) $courseuser->LASTNAME;
                    $user->email = (string) $courseuser->EMAIL;

                    $roles[] = array('role'=>LOCAL_ARCHIVE_TEACHER_ROLE, 'user'=>$user);
                }
            }
        }

        return local_archive_save_course($archive, $coursemaps, $roles);
    }

    public function extract_archive_to_dir($acourseid, $filepath, file_progress $fileprogress = null) {
        // Don't use symnlinks, as the backup conversion code will trigger an error.
        list($res,$ft) = parent::extract_archive_to_dir($acourseid, $filepath, $fileprogress, false);
        if ($res) {
            $moodlexml = "$filepath/moodle.xml";
            // uncompress moodle.xml if necessary
            if (!file_exists($moodlexml) && file_exists($moodlexml.".bz2")) {
                $res = $this->bunzip($moodlexml.".bz2", $moodlexml);
            }
        }

        return array($res,$ft);
    }

}