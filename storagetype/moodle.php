<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/archive/archive_zip_packer.php');

abstract class archive_storagetype_moodle extends archive_storagetype {

    protected $dir;
    protected $storagetype;

    public function __construct($params) {
        parent::__construct($params);
        if (!empty($params['directory'])) { // directory to scan
            $this->dir = $params['directory'];
        } else {
            throw new moodle_exception('archive_storagetype_backup: no directory configured');
        }
        if (!empty($params['roles'])) { // shortnames of roles we are interested in
            $this->roles = $params['roles'];
        } else {
            $this->roles = array('editingteacher');
        }
    }

    abstract protected function process_backup($temppath, $archivename, $archivesize);

    abstract protected function extract($archivepath,$temppath);
    abstract protected function get_backup_mtime($temppath);

    protected function list_archives() {
        $answer = array();
        $handle = @opendir($this->dir);
        if ($handle) {
            while ( ($file=readdir($handle)) !== false ) {
                if (substr($file, 0, 1) === '.') {
                    continue; // skip '.' files
                }
                $answer[] = $file;
            }
        } else {
            throw new moodle_exception('Unable to open: '.$this->dir);
        }
        closedir($handle);
        return $answer;
    }

    public function update_index() {
        global $DB, $OUTPUT;
        $archives = $this->list_archives();
        $archiverecs = $DB->get_records('archive_course', array('archiveid'=>$this->id), '', 'data, id,lastscan');
        $done = array();
        $errors = array();
        foreach ($archives as $filename) {
            $path = $this->dir.'/'.$filename;

            // check the archive mtime
            if (isset($archiverecs[$filename])) {
                $skip = false;
                if (is_file($path)) {
                    $stat = stat($path);
                    if ($archiverecs[$filename]->lastscan >= $stat['mtime']) {
                        $skip = true;
                    }
                    unset($stat);
                } else if (is_dir($path)) {
                    $mtime = $this->get_backup_mtime($path);
                    if ($archiverecs[$filename]->lastscan >= $mtime) {
                        $skip = true;
                    }
                    unset($mtime);
                }
                if ($skip) {
                    $done[] = $archiverecs[$filename]->id;  // mark as processed
                    continue; // archive hasn't changed, so skip
                }
            }

            // process the archive
            $usingtempdir = false;
            try {
                if (is_file($path)) {
                    /* Use temporary directory for expanded zips */
                    $tempid = $this->make_tempdir($path);
                    $usingtempdir = true;
                    $dir = $this->get_tempdir_path($tempid);
                    $this->extract($path,$dir);
                    $archivesize = filesize($path); // This may wrap on a 32-bit server
                } else if (is_dir($path)) {
                    $dir = $path;
                    $archivesize = get_directory_size($path); // This may wrap on a 32-bit server
                } else {
                    throw new moodle_exception('Unknown file type');
                }
                $id = $this->process_backup($dir, $filename, $archivesize);
                $done[] = $id;  // mark as processed
                echo '.';
            } catch (Exception $e) {
                $message = $filename.': '.$e->getMessage();
                if (CLI_SCRIPT) {
                    $errors[] = $message . PHP_EOL;
                } else {
                    $errors[] = '<br />'.$OUTPUT->error_text($message);
                }
                error_log('local/archive/'.$this->storagetype.':'.$message);
            }
            if ($usingtempdir) {
                $this->delete_tempdir($tempid);
            }
            //echo '<!--'.str_pad('', 512).'-->';  // Force the issue of flushing.
                                                 // This is ugly, but I haven't found a better way that works on our setup.
            @ob_flush();
            flush();
        }
        // delete obsolete courses
        $recs = $DB->get_records('archive_course',  array('archiveid'=>$this->id), '', 'id');
        $allids = array_keys($recs);
        $obsolete = array_diff($allids, $done);
        foreach ($obsolete as $acourseid) {
            local_archive_delete_course($acourseid);
        }
        echo PHP_EOL.implode('', $errors);
    }
    public function extract_archive_to_dir($acourseid, $filepath, file_progress $fileprogress = null, $symlink = true) {
        global $DB;
        $filename = $DB->get_field('archive_course', 'data', array('id'=>$acourseid), MUST_EXIST);
        $archivepath = $this->dir.'/'.$filename;
        $ft = filetype($archivepath);
        switch($ft){
            case 'file':
                $fb = get_file_packer();
                $res = ($fb->extract_to_pathname($archivepath, $filepath, null, $fileprogress));
                break;
            case 'dir':
                $res = $this->copy_or_symlink($archivepath, $filepath, $fileprogress, $symlink);
                break;
            default:
                throw new moodle_exception('Unknown file type');
        }
        return array($res,$ft);
    }

}