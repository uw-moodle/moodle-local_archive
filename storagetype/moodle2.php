<?php

defined('MOODLE_INTERNAL') || die();

class archive_storagetype_moodle2 extends archive_storagetype_moodle {

    protected $dir;

    public function get_storagetype() {
        return 'backup2';
    }

    protected function get_backup_mtime($temppath) {
        if (file_exists("$temppath/moodle_backup.xml")) {
            $stat = stat("$temppath/moodle_backup.xml");
        } else if (file_exists("$temppath/moodle_backup.xml.bz2")) {
            $stat = stat("$temppath/moodle_backup.xml.bz2");
        } else {
            $stat = false;
        }
        if ($stat) {
            return $stat['mtime'];
        }
        return false;
    }

    protected function extract($archivepath,$temppath) {
        $zp = new archive_zip_packer();

        $xmls = "moodle_backup.xml,course/enrolments.xml,roles.xml,course/roles.xml,users.xml";
        if(!$zp->extract_file_to_directory($archivepath,$temppath,$xmls)){
            throw new moodle_exception("Failed to extract archive $archivepath");
        }

        return true;
    }
    protected function process_backup($temppath, $archivename, $archivesize) {
        $xmls = array('info'=>array('xml'=>"moodle_backup.xml",'path'=>"information"),
                      'sections'=>array('xml'=>"course/enrolments.xml",'path'=>"enrols"),
                      'roledef'=>array('xml'=>"roles.xml",'path'=>""),
                      'roles'=>array('xml'=>"course/roles.xml",'path'=>"role_assignments"),
                      'users'=>array('xml'=>"users.xml",'path'=>""),
        );
        $simples = array();
        foreach($xmls as $infotype => $info){
            $xml = "$temppath/$info[xml]";
            $simple = false;
            if (file_exists($xml.'.bz2')) {
                $simple = $this->simplexml_load_bzfile($xml.'.bz2');
                if (!$simple) {
                    throw new moodle_exception("Can't load  $xml.bz2");
                }
            } else if (file_exists($xml)) {
                $simple = simplexml_load_file( $xml );
                if (!$simple) {
                    throw new moodle_exception("Can't load  $xml");
                }
            } else {
                if ($infotype == 'users') {
                    // users.xml is missing in backups without user information
                    $simple = new SimpleXMLElement('<users></users>');
                } else if ($infotype == 'sections') {
                    // enrollments.xml is missing in front-page backups
                    $simple = new SimpleXMLElement('<enrolments><enrols></enrols></enrolments>');
                } else {
                    throw new moodle_exception("Can't load  $xml");
                }
            }
            /* Send only desired path in xml */
            if($info['path']){
                $simple = $simple->{$info['path']};
            }
            $simples[$infotype] = $simple;
        }
        return $this->process_moodlexml($simples, $archivename, $archivesize);
    }

    protected function process_moodlexml($simples, $archivename, $archivesize) {
        global $DB;

        $archive = new stdClass();
        $archive->archiveid = $this->id;
        $archive->data = $archivename;
        $archive->coursename = (string) $simples['info']->original_course_fullname;
        $archive->archivedate = (string) $simples['info']->backup_date;
        $archive->archivesize = $archivesize;
        $archive->lastscan = time();

        // coursemaps
        $coursemaps = array();
        if ($simples['sections']) {
            foreach ($simples['sections']->enrol as $enrolment) {
                if(!($sections = $enrolment->plugin_enrol_wisc_enrol)){
                    continue;
                }
                foreach($sections->coursemaps->coursemap as $section){
                    $coursemap = new stdClass();
                    $coursemap->term = (string) $section->term;
                    $coursemap->subject_code = (string) $section->subject_code;
                    $coursemap->catalog_number = (string) $section->catalog_number;
                    $coursemap->section_number = (string) $section->section_number;
                    $coursemap->session_code = (string) $section->session_code;
                    $coursemap->class_number = (string) $section->class_number;
                    $coursemaps[] = $coursemap;
                }
            }
        }

        //roles
        $teacherrole = false;
        if ($simples['roledef']){
            /* Find teacher role id */
            foreach($simples['roledef']->role as $role){
                if($role->archetype == 'editingteacher'){
                    $teacherrole = (string)$role->attributes()->id;
                    break;
                }
            }
        }
        $userids = array();
        if($teacherrole && $simples['roles']) {
            /* Get all teacher role assignments */
            foreach($simples['roles']->assignment as $assign){
                if($assign->roleid == $teacherrole){
                    $userids[(int)$assign->userid] = true;
                }
            }
        }

        //users
        $roles = array();
        if (!empty($userids) && $simples['users']) {
            foreach ($simples['users']->user as $courseuser) {
                $userid = (int)$courseuser->attributes()->id;
                if(!isset($userids[$userid])){
                    continue;    //Skip non-teacher users
                }
                $user = new stdClass();
                $user->pvi = (string) $courseuser->idnumber;
                if(strpos($courseuser->username,'@')){
                    $user->netid = strstr($courseuser->username, '@', true);
                }else{
                    $user->netid = (string) $courseuser->username;
                }
                $user->firstname = (string) $courseuser->firstname;
                $user->lastname = (string) $courseuser->lastname;
                $user->email = (string) $courseuser->email;

                $roles[] = array('role'=>LOCAL_ARCHIVE_TEACHER_ROLE, 'user'=>$user);
            }
        }

        return local_archive_save_course($archive, $coursemaps, $roles);
    }

    public function extract_archive_to_dir($acourseid, $filepath, file_progress $fileprogress = null) {
        $res = parent::extract_archive_to_dir($acourseid, $filepath, $fileprogress);
        if ($res) {
            // uncompress moodle.xml if necessary
            $moodlexml = "$filepath/moodle.xml";
            if (!file_exists($moodlexml) && file_exists($moodlexml.".bz2")) {
                $res = $this->bunzip($moodlexml.".bz2", $moodlexml);
            }
        }
        return $res;
    }
}
