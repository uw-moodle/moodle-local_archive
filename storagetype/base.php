<?php

defined('MOODLE_INTERNAL') || die();

abstract class archive_storagetype {

    protected $id;
    protected $name;


    public function __construct($params) {
        global $DB;
        if (!empty($params['name'])) { // unique identifier for db
            $instancename = $params['name'];
        } else {
            throw new moodle_exception("No instance name configured");
        }
        $this->name = $params['name'];
        $storagetype = $this->get_storagetype();
        $id = $DB->get_field('archive', 'id', array('name'=>$instancename, 'storagetype'=>$storagetype));
        if ($id === false) {
            $instance = new stdClass();
            $instance->name = $instancename;
            $instance->storagetype = $storagetype;
            $id = $DB->insert_record('archive', $instance);
        }
        $this->id = $id;
    }

    abstract public function update_index();
    abstract function get_storagetype();
    abstract function extract_archive_to_dir($archiveid, $filepath, file_progress $fileprogress = null);

    public function get_id() {
        return $this->id;
    }
    public function get_name() {
        return $this->name;
    }
    protected function make_tempdir($path) {
        $tempid = md5($path.time());
        $dirname = $this->get_tempdir_path($tempid);
        $res = mkdir($dirname, 0777, true);
        if (!$res) {
            throw new moodle_exception("Can't make directory $dirname");
        }
        return $tempid;
    }
    protected function get_tempdir_path($tempid) {
        global $CFG;
        return $CFG->tempdir."/archive/".$tempid;
    }

    protected function delete_tempdir($tempid) {
        global $CFG;

        $status = true;
        $dirname = $this->get_tempdir_path($tempid);
        $status = self::delete_dir_contents($dirname);
        if ($status) {
            $status = rmdir($dirname);
        }
        return $status;
    }

    protected function copy_or_symlink($archivepath, $filepath, file_progress $fileprogress = null, $symlink = true) {
        // A top-level symlink would be faster, but it causes
        // problems when moodle deletes the temp/backup directory.
        if (false && PHP_OS == "Linux") {
           return symlink($archivepath, $filepath);
        } else {
            return self::copyr($archivepath, $filepath, $fileprogress, $symlink);
        }
    }

    protected function simplexml_load_bzfile($filename) {
        $fh = bzopen($filename, 'r');
        if (!$fh) {
            return false;
        }
        $data = array();
        while (!feof($fh)) {
          $data[] = fread($fh, 8192);
        }
        bzclose($fh);
        $data = implode($data);
        return simplexml_load_string($data);
    }

    protected function bunzip($path, $newpath) {
      $bz=bzopen($path, 'r');
      $fp=fopen($newpath, 'wb');
      if (!$bz || !$fp) {
          return false;
      }
      while (!feof($bz)) {
          fwrite($fp, bzread($bz, 4096));
      }
      bzclose($bz);
      fclose($fp);
      return true;
    }

    /**
     * Copy a file, or recursively copy a folder and its contents
     *
     * @author      Aidan Lister <aidan@php.net>
     * @version     1.0.1
     * @link        http://aidanlister.com/repos/v/function.copyr.php
     * @param       string   $source    Source path
     * @param       string   $dest      Destination path
     * @return      bool     Returns TRUE on success, FALSE on failure
     */
    protected static function copyr($source, $dest, file_progress $fileprogress = null, $symlink = true)
    {
        global $CFG;

        // Simple copy for a file.  Link if possible
        if (is_file($source)) {
            if ($symlink && PHP_OS == "Linux") {
                return symlink($source, $dest);
            } else {
                return copy($source, $dest);
            }
        }

        // Make destination directory
        if (!is_dir($dest)) {
            mkdir($dest, $CFG->directorypermissions, true);
        }

        // Loop through the folder
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            // Deep copy directories
            if ($dest !== "$source/$entry") {
                self::copyr("$source/$entry", "$dest/$entry", $fileprogress, $symlink);
            }
        }

        // Clean up
        $dir->close();
        return true;
    }

    /**
     * Given one fullpath to directory, delete its contents recursively
     * Copied originally from somewhere in the net.
     * TODO: Modernise this
     */
    protected static function delete_dir_contents($dir, $excludeddir='') {
        global $CFG;

        if (!is_dir($dir)) {
            // if we've been given a directory that doesn't exist yet, return true.
            // this happens when we're trying to clear out a course that has only just
            // been created.
            return true;
        }
        $slash = "/";

        // Create arrays to store files and directories
        $dir_files      = array();
        $dir_subdirs    = array();

        // Make sure we can delete it
        chmod($dir, $CFG->directorypermissions);

        if ((($handle = opendir($dir))) == false) {
            // The directory could not be opened
            return false;
        }

        // Loop through all directory entries, and construct two temporary arrays containing files and sub directories
        while (false !== ($entry = readdir($handle))) {
            if (is_dir($dir. $slash .$entry) && $entry != ".." && $entry != "." && $entry != $excludeddir) {
                $dir_subdirs[] = $dir. $slash .$entry;

            } else if ($entry != ".." && $entry != "." && $entry != $excludeddir) {
                $dir_files[] = $dir. $slash .$entry;
            }
        }

        // Delete all files in the curent directory return false and halt if a file cannot be removed
        for ($i=0; $i<count($dir_files); $i++) {
            chmod($dir_files[$i], $CFG->directorypermissions);
            if (((unlink($dir_files[$i]))) == false) {
                return false;
            }
        }

        // Empty sub directories and then remove the directory
        for ($i=0; $i<count($dir_subdirs); $i++) {
            chmod($dir_subdirs[$i], $CFG->directorypermissions);
            if (self::delete_dir_contents($dir_subdirs[$i]) == false) {
                return false;
            } else {
                if (remove_dir($dir_subdirs[$i]) == false) {
                    return false;
                }
            }
        }

        // Close directory
        closedir($handle);

        // Success, every thing is gone return true
        return true;
    }
}