<?php

/**
 * Updates archive index
 */

require('../../config.php');
require_once($CFG->dirroot.'/local/archive/locallib.php');
require_once($CFG->dirroot.'/local/archive/scan_forms.php');

$PAGE->set_url('/local/archive/scan.php');
$PAGE->set_context(context_system::instance());

require_login();

require_capability('moodle/site:config', context_system::instance());

// Try to get flush() to work
//@apache_setenv('no-gzip', 1);
@ini_set('zlib.output_compression', 0);

$PAGE->set_pagelayout('admin');
$PAGE->set_heading('Rescan archives');
$PAGE->set_title('Rescan archives');

$scan = optional_param('scan', 0, PARAM_BOOL);

local_archive_init_archives();
$mform = new archive_scan_form(null, array('archives'=>$LOCAL_ARCHIVE_INSTANCES), 'post');

if ($mform->is_cancelled()) {
    redirect($CFG->wwwroot);
} else if ($data = $mform->get_data()) {
    $archiveids = array();
    foreach ((array)$data as $fieldname=>$value) {
        if (strpos($fieldname, 'archive_') !== 0) {
            continue;
        }
        list($ignored, $archiveid) = explode('_', $fieldname);
        $archiveids[] = $archiveid;
    }

    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('rebuildindex', 'local_archive'));
    foreach ($archiveids as $archiveid) {
        $archive = $LOCAL_ARCHIVE_INSTANCES[$archiveid];
        echo "<b>Updating ".$archive->get_name()." (".$archive->get_storagetype().")</b>";
        echo "<div style='width:80%;margin-left:1em;'><p style='word-wrap:break-word;'>";
        $archive->update_index();
        echo "</p></div>";
        $count = $DB->count_records('archive_course', array('archiveid'=>$archive->get_id()));
        echo "Total courses: $count<hr />\n";
    }
    echo "<br /><h2>Done!</h2>";

    echo $OUTPUT->continue_button('/');
    echo $OUTPUT->footer();
    exit;
}

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('rebuildindex', 'local_archive'));

$mform->display();
echo $OUTPUT->footer();
