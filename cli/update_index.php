<?php

/**
 * CLI script to update archive index
 *
 * Usage: php update_index.php [name1] [name2] [...]
 */

define('CLI_SCRIPT', true);

require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');
require_once($CFG->dirroot.'/local/archive/locallib.php');

//

// Ensure errors are well explained
$CFG->debug = DEBUG_NORMAL;

$instances = array_slice($argv, 1);
local_archive_update_index($instances);

